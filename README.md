MachoBear Test Assignment
=========================

This is a challenge test for MachoBear studio. 
Project is based on React and uses the following libraries:
 * jQuery
 * Openlayers Maps 
 * Bootstrap3

All styles are compiled from SASS on fly. 

Installation
------------

run `npm install`

Running
-------

run `npm start` and open [http://localhost:8080](http://localhost:8080) URL in your browser.