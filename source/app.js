'use strict';

import React, {PropTypes, Component} from 'react'
import ReactDOM from 'react-dom'

import Header from './components/header/Header'
import Footer from './components/footer/Footer'
import Carousel from './components/carousel/Carousel'
import Mobile from './components/content/Mobile'
import Works from './components/content/Works'
import Team from './components/content/Team'
import About from './components/content/About'

const CAROUSEL_DATA = [
    { src: '/images/carousel-1.jpg'},
    { src: '/images/carousel-2.jpg'}
];

class App extends Component {
    render() {
        return (
            <div className='container-fluid'>
                <div className='row'>
                    <Header />
                </div>
                <div className='row'>
                    <Carousel data={CAROUSEL_DATA} />
                </div>
                <div className='row'>
                    <Mobile />
                </div>
                <div className='row'>
                    <Works />
                </div>
                <div className='row'>
                    <Team />
                </div>
                <div className='row'>
                    <About />
                </div>
                <div className='row'>
                    <Footer />
                </div>
            </div>
        )
    }
}

ReactDOM.render(<App />, document.body, () => {
  require('./tests/tests').run();
});

