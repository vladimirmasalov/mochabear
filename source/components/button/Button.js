'use strict'

import React, {PropTypes, Component} from 'react'
import './Button.scss'

const propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    label: PropTypes.string
};

class Button extends Component {
    render() {
        return (
            <div className={"btn noselect " +this.props.className} onClick={this.props.onClick}>{this.props.label}</div>
        )
    }
}

Button.propTypes = propTypes;

export default Button;