'use strict';

import React, {PropTypes, Component} from 'react'
import Button from '../button/Button'
import './Carousel.scss'

const propTypes = {
    className: PropTypes.string,
    data: PropTypes.array
};

class Carousel extends Component {
    handleLeft() {
        console.log(1);
    } // handleLeft

    handleRight() {
        console.log(2);
    } // handleRight

    render() {
        let items = this.props.data.map((item, index) => {
            return <div key={item.src} className={'item' + (index === 0 ? ' active': '')}>
                        <div className='img' style={{'backgroundImage':'url('+item.src+')'}} ></div>
                   </div>
        });
        return (
            <section id='carousel' className='carousel slide' data-ride='carousel'>
                <div className='carousel-inner' role='listbox'>
                    {items}
                </div>

                <a className="left carousel-control" href="#carousel" role="button" data-slide="prev">
                    <span className="glyphicon glyphicon-menu-left icon-prev" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="right carousel-control" href="#carousel" role="button" data-slide="next">
                    <span className="glyphicon glyphicon-menu-right icon-next" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </section>
        ); // return
    } // render
} // Footer

Carousel.propTypes = propTypes;

export default Carousel