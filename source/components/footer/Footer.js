'use strict';

import React, {PropTypes, Component} from 'react'
import './Footer.scss'

const MENU = [
    {id: 'footer-1', label: "Product", url: "/product"},
    {id: 'footer-2', label: "Work", url: "/works"},
    {id: 'footer-3', label: "Team", url: "/team"},
    {id: 'footer-4', label: "About", url: "/about"},
    {id: 'footer-5', label: "Contact Us", url: "/contact"}
]

export default class Footer extends Component {
    render() {
        let items = MENU.map((item) => {
            return <div key={item.id} className='col-md-2'><a href={item.url}>{item.label}</a></div>
        });
        return (
            <footer>
                <div className='col-sm-12'>
                    <span className='logo'>MACHO</span>
                </div>
                <div className='col-sm-12'>
                    <hr />
                </div>
                <nav className='col-md-7'>{items}</nav>
            </footer>
        ); // return
    } // render
} // Footer