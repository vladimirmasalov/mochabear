'use strict';

import React, {PropTypes, Component} from 'react'
import Button from '../button/Button'
import './Mobile.scss'

export default class Mobile extends Component {
    onClick() {
        alert('DOWNLOAD APP CLICK');
    }
    render() {
        return (
            <section className='mobile container-fluid'>
                <div className='row'>
                    <div className='col-md-1'></div>
                    <div className='col-md-5'>
                        <h1>The Macho App</h1>
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.
                        </p>
                        <Button label='DOWNLOAD THE APP' onClick={this.onClick.bind(this)} />
                    </div>
                    <div className='col-md-6'></div>
                </div>
            </section>
        ) // return
    } // render
} // Mobile