'use strict';

import React, {PropTypes, Component} from 'react'
import './Team.scss'

const DATA = [
    { id: 'team-1', html: '<h4>FirstName LastName</h4><p>CEO</p>'},
    { id: 'team-2', html: '<h4>FirstName LastName</h4><p>Designer</p>'},
    { id: 'team-3', html: '<h4>FirstName LastName</h4><p>Admin</p>'},
    { id: 'team-4', html: '<h4>FirstName LastName</h4><p>Cool lady</p>'},
    { id: 'team-5', html: '<h4>FirstName LastName</h4><p>Doggy</p>'}
]

export default class Team extends Component {
    render() {
        let items = DATA.map((obj) => {
            return <div key={obj.id} className='team-content col-md-15'>
                        <div className='img'></div>
                        <summary dangerouslySetInnerHTML={{__html: obj.html}}></summary>
                    </div>
        })
        return (
            <section className='team container-fluid'>
                <div className='row'>
                    <div className='title col-xs-12'><span>Our Team</span></div>
                </div>
                <div className='row'>
                    {items}
                </div>
            </section>
        ) // return
    } // render
} // Team