'use strict';

import React, {PropTypes, Component} from 'react'
import Button from '../button/Button'
import Map from '../map/Map'
import './About.scss'

export default class About extends Component {
    onClick() {
        alert('Contact Us');
    }
    render() {
        return (
            <section className='about container-fluid'>
                <div className='row'>
                    <div className='col-md-6'>
                        <h1>About Us</h1>
                        <p>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."</p>
                        <Button label='CONTACT US' onClick={this.onClick.bind(this)} />
                    </div>
                    <div className='col-md-6'>
                        <Map lat='49.26983976593348' lon='-123.1107314069187'/>
                    </div>
                </div>
            </section>
        ) // return
    } // render
} // About