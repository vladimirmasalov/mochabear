'use strict';

import React, {PropTypes, Component} from 'react'
import './Works.scss'

const DATA = [
    { id: 'works-1', html: '<h4>Project One</h4><br/><p>Rollover text 1</p>'},
    { id: 'works-2', html: '<h4>Project Two</h4><br/><p>Rollover text 2</p>'},
    { id: 'works-3', html: '<h4>Project Three</h4><br/><p>Rollover text 3</p>'},
    { id: 'works-4', html: '<h4>Project Four</h4><br/><p>Rollover text 4</p>'},
    { id: 'works-5', html: '<h4>Project Five</h4><br/><p>Rollover text 5</p>'},
    { id: 'works-6', html: '<h4>Project Six</h1><br/><p>Rollover text 6</p>'}
]

export default class Works extends Component {
    render() {
        let projects = DATA.map((obj) => {
            return <div key={obj.id} className={obj.id+' works-content col-sm-4'}>
                        <div className='img'></div>
                        <summary dangerouslySetInnerHTML={{__html: obj.html}}></summary>
                    </div>
        })
        return (
            <section className='works container-fluid'>
                <div className='row'>
                    <div className='works-header col-xs-12'>
                        <span className='title'>Our Works</span>
                        <span className='text right'>52 works in total</span>
                    </div>
                </div>
                <div className='row'>
                    {projects}
                </div>
            </section>
        ) // return
    } // render
} // Works