'use strict';

import React, {PropTypes, Component} from 'react'
import './Header.scss'

const MENU = [
    {id: 'header-1', label: "PRODUCT", url: "/product"},
    {id: 'header-2', label: "WORKS", url: "/works"},
    {id: 'header-3', label: "TEAM", url: "/team"},
    {id: 'header-4', label: "ABOUT", url: "/about"},
    {id: 'header-5', label: "CONTACT", url: "/contact"}
]

export default class Header extends Component {
    render() {
        let items = MENU.map((item, index) => {
            return <div key={item.id} className={(index === 4 ? 'col-sm-3' : 'col-sm-2')}><a href={item.url}>{item.label}</a></div>
        });
        return (
            <header>
                <div className='col-sm-2'>
                    <span className='logo'>MACHO</span>
                </div>
                <div className='col-sm-3 col-md-4'></div>
                <nav className='right col-sm-7 col-md-6'>
                    {items}
                </nav>
            </header>
        )
    }
}