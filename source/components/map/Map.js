'use strict';

import React, {PropTypes, Component} from 'react'
import OpenLayer from 'openlayers'
import './Map.scss'

const propTypes = {
    lat: PropTypes.string,
    lon: PropTypes.string
}

class Map extends Component {
    componentDidMount() {
        let coordinates = OpenLayer.proj.fromLonLat([parseFloat(this.props.lon), parseFloat(this.props.lat)]);
        let overlay = new OpenLayer.Overlay({ element: this.refs.popup });
        let map = new OpenLayer.Map({
            layers: [
                new OpenLayer.layer.Tile({
                    source: new OpenLayer.source.OSM()
                })
            ],
            target: 'map',
            overlays: [
                overlay
            ],
            view: new OpenLayer.View({
                center: coordinates,
                zoom: 17
            })
        });

        // Set position of the overlay;
        overlay.setPosition(coordinates);
    } // componentDidMount

    render() {
        return (
            <div className='map-wrapper'>
                <div className='map-popup' ref='popup'>
                    <div className='map-popup-arrow'></div>
                    <p>1716 Cook Street
                    <br />Vancouver, BC, V5Y 3J6
                    <br /><br />hello@ottono.com
                    <br /><br />+1 650-253-0000
                    </p> 
                </div>
                <div className='map' id='map'></div>
            </div>
        ) // return
    } // render
} // Map

Map.propTypes = propTypes;

export default Map