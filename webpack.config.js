var path = require('path'),
    webpack = require('webpack'),
    ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: path.resolve(__dirname, 'source/app.js'),

  output: {
    path: path.resolve(__dirname, '/__build__'),
    filename: 'bundle.js'
  },

  resolve: {
    extensions: ['', '.js', '.jsx', '.ts']
  },

  module: {
    loaders: [
      { test: /\.json$/, loader: 'json-loader' },
      { test: /\.js$/, loader: 'jsx-loader?harmony' },
      { test: /\.js?$/, exclude: /node_modules/, loader: 'babel-loader'},
      { 
          test: /\.(ttf|eot|svg|woff2?)(\?v=[a-z0-9=\.]+)?$/i,
          exclude: /node_modules/,
          loader: 'file?name=fonts/[name].[ext]'
      },
      {
          test: /\.(jpe?g|png|gif|svg|ico)$/i,
          exclude: /node_modules/,
          loaders: [
              'file?name=images/[sha512:hash:base64:7].[ext]',
              'image-webpack?progressive=true&optimizationLevel=7&interlaced=true'
          ]
      },
      {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract('css!sass', {
            allChunks: true
          })
      }, 
      {
          test: require.resolve('openlayers'),
          loader: 'imports?define=>false'
      }
    ],
  }, 

  plugins: [
    new webpack.optimize.CommonsChunkPlugin('shared.js'),
    new ExtractTextPlugin("styles.css")
  ]
};